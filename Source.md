### Download:
- https://godotengine.org/download/linux/
- https://flathub.org/apps/org.godotengine.Godot
- https://snapcraft.io/gd-godot-engine-snapcraft

Download the 4.2 version

## Tips
- [Officials Doc](https://docs.godotengine.org/en/stable/)
- [Great tool zero to hero in gdscript](https://gdquest.github.io/learn-gdscript/)
- [GDquest Youtube chanel](https://www.youtube.com/@Gdquest/videos)
- [presentation of Godot in 100 seconds video](https://www.youtube.com/watch?v=QKgTZWbwD1U&pp=ygUNd2hhdCBpcyBnb2RvdA%3D%3D)
## Source
